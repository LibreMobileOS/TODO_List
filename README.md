###### Copyright 2020 - The LibreMobileOS Foundation

# LibreMobileOS TO-DO List

## Technical goals

### Concepts to Learn and document:

* HAL & HIDL
* Selinux
* AIDL
* APEX
* VINTF
* DT – DTB – DTBO
* VNDK
* bootloader flow
* writing kernel driver

### AOSP device-specific

* Track CVE's and apply kernel patches for all devices (1 month)
* Keep all device trees update (continuous)
* Start supporting new devices that require device tree and document everything(10 new devices every 3 months)
* Current devices bug fixing (continuous)

### AOSP Platform

* Add new features on top of lineage (1 month)
* Add security patches to framework (2 month)
* UI improvements (1 month)
* Keep track of google SPL and update our repos every month (happens once in a month)
* Update to new android versions and rebase all features every year (happens once in a year)

### AOSP GSI

* Setup GSI patches and build image (1week)
* Write HAL to HAL implementations to replace dirty framework hacks (2 month)
* Fix IMS and other closed source system implementations (1 month)
* Support new devices and online bug fixes (continuous)
* Help platform dev (continuous)

### Hybris-mobian

#### Mobian device side:
* Build rootfs using debos (1 day)
* Fix phosh gnome center crash bug (1 week)
* Add color management support for hwc2 on wlroots (1 day)
* Add support for Audio (1 day)
* Add support for RIL (1 week)
* Add support for Bluetooth (1 day)
* Add support for Sensors (1 day)
* Add support for Media (2 week)
* Add support for Lights (3 day)
* Add support for Vibrator (3 day)
* Add support for GPS (2 week)
* Add support for Camera (1 month)
* Add support for Power(Interactive mode) (1 month)
* Add support for Auto brightness (?)
* Add support for Fingerprint (1 month)
  * Take Ubports biometryd and add UI to gnome settings
* Add support for Flashlight using camera hal (2 week)
  * Add support for NFC (2 week)

#### Mobian platform side:
* Phosh UI improvements (1 month)
  * Blur in backgroud apps list
  * More animations
* Brandings (1 week)
* Plasma support (2 week)
* Add xf86 support (3 day)
* Write a new phone grateer with touch support for lightDM (1 month)
  * With desktop env selection support

### Ubports:
* Start learning clickable platform and QML to write apps for Ubuntu touch(?)
* Upstream GSI patches (3 days)
  * Upstream repowerd patches
  * Move overlay-reader to deviceinfo
  * Make use of binderized hfd service
* Build halium-boot for our supported devices (1 week)
  * Redmi note 8
  * Galaxy a20s
* Fix auto brightness and flashlight using camera hal (2 week)
* Fix FOD (2 month)
  * Use lineage fod hal
  * Draw circle and dim layer using mir
* Add Lomiri support to Mobian (2 month)
  * Need to check manjaro arm patches
* Add Halium10 support with help of Sailfish dev (1 week)
* Fix gst-droid camera video recording bug (1 week)
* Update Anbox from 7 to 11 (2 month)
  * (Need android knowledge)
  * Study chromium OS android container 
  * With help of ubports people

### Sailfish
* Move all packages to Halium (2 week)
* Make GSI of it (2 week)
* Port for our supported devices (1 week)
  * Check GSI on our devices and debug
* Add nemo UI support to Mobian (forever)
  * (Is it even possible?)
* Add hybris10 support with help of Ubports dev (2 month)
  * Upstream hybris-patches
  * Update libhybris
  * Update userspace drivers

## Non-technical goals

### Website
* Develop LMO website containing 4 major parts as follow:
1. News and events
1. Request new device support
1. Wiki and downloads
1. Developers and users forums

* Create a YouTube channel for storing our tutorials (i.e. flashing ROMs, installing recovery images, etc.)
* Create at least 6 branches in 6 different countries that help develop our products and localize them
* Hold our first online conference with the purpose of presenting our achievements and answering developers’ and users’ questions.
